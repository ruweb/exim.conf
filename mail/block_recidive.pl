#!/usr/bin/perl
# grep block_recidive /etc/crontab || echo '51 23 * * * mail perl /etc/mail/block_recidive.pl 2>&1 | mail -Es "crontab: perl /etc/mail/block_recidive.pl" root' >> /etc/crontab

my $num_days=3;
my $hit_dir="/etc/virtual/usage_hit";
my $spool_directory="/var/spool/exim";
my $eximpl="/etc/exim.pl";

do $eximpl;

my $user,$domain;
my %blocked=();
my @time= localtime(time);
my $date = join('',(@time[3],@time[4]+1,@time[5]-100));
my $script_block=0;
if (open (FH, "/etc/exim.conf")) {
    $script_block = grep { m#/ALL-BLOCKED\}lsearch\{\$spool_directory/blocked_script_paths# } <FH>;
    close FH;
}
my @arr;

-d $hit_dir || mkdir $hit_dir; # && chmod (0775, $hit_dir);

foreach (grep {-M > $num_days-0.1} glob "$hit_dir/*") { unlink; }

foreach (glob "/etc/virtual/*/") { foreach (glob $_."usage/*") {
    ($domain,$user) = m#/etc/virtual/([^/]+)/usage/(.*)#;
    !-l "/etc/virtual/$domain" || next;
    if (hit_limit_email($user,$domain)>0){
        open (FH, ">$hit_dir/$domain\@$user.$date") && close FH;
        @arr=glob "$hit_dir/$domain\@$user.*";
        if ($num_days <= @arr ) {
            recidive_block('smtp',"$user\@$domain");
            $user=get_domain_owner($domain);
            $blocked{$user}=$blocked{$user}+1;
        }
    }
}}

foreach (glob "/etc/virtual/usage/*"){
    m/\./ && next;
    ($user) = m#/etc/virtual/usage/(.*)#;
    if (hit_limit_user($user)>0){
        open (FH, ">$hit_dir/$user.$date") && close FH;
        @arr=glob "$hit_dir/$user.*";
        if (!$blocked{$user} && $num_days <= @arr ) {
            my $smtp_count=0;
            my $script_count=0;
            open FH, "/etc/virtual/usage/$user.bytes";
            while (<FH>) {
                my %hash=();
                my $str = (split /=/,$_,2)[1];
                foreach (split /&/,$str) {
                    my ($key,$val) = split /=/,$_,2;
                    $hash{$key}=$val;
                }
                $hash{'method'} eq 'incoming' && next;
                $hash{'authenticated_id'} eq $user || next;
                if ($hash{'sender_host_address'}){ $smtp_count++; }
                    else { $script_count++; }
            }
            close FH;
            if ($smtp_count>$script_count) {
                recidive_block('smtp',$user);
            } else {
                recidive_block(($script_block?'script':'user'),$user);
            }
            close FH;
        }
    }
}

sub recidive_block {
        my($bc_type,$authenticated_id) = @_;

        my $script_path         = "";
        my $sender_host_address = "";
        my $mid                 = "";
        my $timestamp           = time();

        if ($bc_type eq "script") {
            $script_path = "/home/$authenticated_id/ALL-BLOCKED";
            open(TQ, ">>$spool_directory/blocked_script_paths");
            print TQ "$script_path:$timestamp:reason=recidive\n";
            close(TQ);
        } elsif ($bc_type eq "smtp") {
            $sender_host_address = "0.0.0.0";
            open(TQ, ">>$spool_directory/blocked_authenticated_users");
            print TQ "$authenticated_id:$timestamp:reason=recidive\n";
            close(TQ);
        }
        if ($bc_type eq "user") {
            open(TQ, ">>/etc/virtual/blacklist_usernames");
            print TQ "$authenticated_id:$timestamp:reason=recidive\n";
            close(TQ);
        } else {
            open(TQ, ">>/etc/virtual/mail_task.queue");
            print TQ "action=block_cracking&type=$bc_type&authenticated_id=$authenticated_id&script_path=$script_path&sender_host_address=$sender_host_address&log_time=$timestamp\n";
            close(TQ);
        }
#        print "$authenticated_id $bc_type blocked\n";
}
